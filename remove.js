window.onload = function() {

    createBtn();
    var keywords = [
        'QQ', 'qq', 'Qq', 'qQ', '薇商', 'ye业', 'ye务', '夏面', 'vx:No', '约P', '+V', '+扣扣', '+薇', '加粉', '万紛', 'vx:',
        '加我微信', '加微信'
    ];
    localStorage.setItem('keywords', JSON.stringify(keywords));
    //
    // if (!localStorage.getItem('keywords')) {
    //     var keywords = [
    //         'QQ', 'qq', 'Qq', 'qQ', '薇商', 'ye业', 'ye务', '夏面', 'vx:No', '约P', '+V', '+扣扣', '+薇', '加粉', '万紛', 'vx:'
    //     ];
    //     localStorage.setItem('keywords', JSON.stringify(keywords));
    // }

    console.log(localStorage.getItem('keywords'));
};

function createBtn() {
    var parentDom = $('.WB_tab_b');
    if(parentDom.length == 0) {

        setTimeout(function (args) {
            createBtn();
        }, 500);

        return ;
    }
    
    parentDom.append("<button style='margin-left:16px;height:26px;cursor:pointer' class='W_btn_b' id='clean_current'>清理本页僵尸粉</button>");
    parentDom.append("<button style='margin-left:16px;height:26px;cursor:pointer' class='W_btn_b' id='clean_all'>清理所有僵尸粉</button>");

    document.getElementById("clean_current").addEventListener('click', function() {
        cleanCurrentPageFans();
    });

    document.getElementById("clean_all").addEventListener('click', function() {
        cleanAllFans();
    });
}

function cleanCurrentPageFans() {
    var _confirm = confirm("确认清理吗？没有发过微博的粉丝，或者最后一条微博内有相应关键词的，都会被移除！");

    if (!_confirm)
        return ;

    hideWBTab();

    var bufFollowList = $(".follow_list").find('li');
    var followList = [];

    for (var i = 0; i < bufFollowList.length; i++) {

        var _this = $(bufFollowList[i]);
        var items = _this.find(".item");

        if (items.length != 0) {
            followList.push(items);
        }
    }

    setTimeout(function () {

        getFans(followList, 0);
    }, 3000);
}

function getFans(followList, x) {

    if (x >= followList.length) {

        changeHideText("结束");
        return ;
    }

    var items = followList[x];

    for (var i = 0; i < items.length; i++) {

        if (items[i].innerText == '移除粉丝')
            detect($(items[i]).find('a').attr('action-data'));
    }

    setTimeout(function () {

        getFans(followList, (x + 1));
    }, 3000);
}

function cleanAllFans() {
    var _confirm = confirm("确认清理吗？没有发过微博的粉丝，或者最后一条微博内有相应关键词的，都会被移除！");

    if (!_confirm)
        return ;

    alert("防止不可逆误伤，先不开放");
}

function detect(actionDataStr) {

    //解析该str，格式为： uid=123456&fname=XXX輽&fnick=XXX
    //解析出uid、fname、fnick
    var uid;
    var fName;
    var fNick;
    var isZombie = false;

    //先按照&分割
    var _bufArr = actionDataStr.split("&");

    for (var i = 0; i < _bufArr.length; i++) {

        //按照=号分割
        var _bufParameter = _bufArr[i].split("=");

        if (_bufParameter.length < 2)
            return ;

        switch (_bufParameter[0]) {
            case 'uid':
                uid = _bufParameter[1];
                break;
            case 'fname':
                fName = _bufParameter[1];
                break;
            case 'fnick':
                fNick = _bufParameter[1];
                break;
            default:
                return ;
        }
    }

    if (!uid || !fName || !fNick)
        return ;

    //修改loading提示
    changeHideText("正在检测"+fNick+"是不是僵尸");
    // console.log("正在检测"+fNick+"是不是僵尸");
    
    //获取该用户最新的微博
    var lastWeibo = getUserLastWeibo(uid);

    if (lastWeibo === false) {
        addErrorLog("用户 "+fNick+" 请求失败");
        return ;
    }

    // console.log(lastWeibo);

    if (lastWeibo === true) {
        //这个B肯定是，不用犹豫
        isZombie = true;
    } else {
        //对比一下关键词库再看看他是不是
        isZombie = compareKeywords(lastWeibo);
    }

    if (isZombie) {
        //干他
        // console.log("干他："+fNick);
        remove(uid, fName, fNick);
        addSuccessLog(fNick+ " 已被移除");
    }
}

function remove(uid, fName, fNick) {

    $.ajax({
        url: 'https://weibo.com/aj/f/remove',
        type: 'POST',
        async: false,
        data: {
            uid: uid,
            fname: fName,
            fnick: fNick,
            _t: 0
        },
        success: function (data) {

            console.log(data);
        },
        error: function (err) {

            console.log(err);
        }
    })
}

function compareKeywords(str) {

    var arr = JSON.parse(localStorage.getItem('keywords'));

    for (var i = 0; i < arr.length; i++) {

        if (str.indexOf(arr[i]) !== -1) {
            // console.log(str+" 中有关键词 "+ arr[i]);
            return true;
        }
    }

    return false;
}

function getUserLastWeibo(uid) {

    var url = "https://weibo.com/u/"+uid+"?&is_all=1";
    var res = false;

    $.get({
        url: url,
        async: false,
        success: function (data) {

            var doms = $(data);

            //从后往前找，找到最后一个script标签
            for (var i = doms.length - 1; i >=0; i--) {

                if (doms[i].localName == 'script') {

                    var innerText = doms[i].innerText;

                    //这个标签内必须有content.homeFeed.index字符串
                    if (innerText.indexOf('content.homeFeed.index') !== -1) {
                        //截取出JSON格式的html数据
                        try {
                            var viewJson = JSON.parse(innerText.slice(8, innerText.length - 1));

                            if (viewJson.html) {

                                //再将html解析出来
                                var feed = $(viewJson.html);

                                // console.log(feed);
                                if (feed) {

                                    //找出最上面的一条微博
                                    var last = feed.find(".WB_text");

                                    // console.log(last);

                                    if (last.length < 1) {

                                        //这个B连1条微博都没有，搞死他
                                        res = true;
                                    } else {

                                        last = last[0];

                                        // console.log(last);
                                        // console.log($(last));
                                        // console.log($(last)[0]);
                                        res = $(last)[0].innerText.replace(/\s+/g,"");
                                    }
                                }
                            }

                        } catch (err){

                            console.log(err);

                            //直接把整个字符串去比对关键词，如果JSON解不出来的话

                            //避免误伤，严格过滤一下
                            res = filterStr(innerText);
                        }
                    }

                    break;
                }
            }
        }
    });

    return res;
}

function filterStr(str) {
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%+_]");
    var specialStr = "";
    for (var i = 0; i < str.length; i++) {
        specialStr += str.substr(i, 1).replace(pattern, '');
    }
    return specialStr.replace(/\s+/g,"");   //顺便干掉空格
}

function changeHideText(txt) {

    $("#clean-loading").text(txt);
}

function addErrorLog(str) {

    $("#error_box").append("<div>"+str+"</div>");
}

function addSuccessLog(str) {
    $("#success_box").append("<div>"+str+"</div>");
}

function hideWBTab() {
    $("#clean_current").css('display', 'none');
    $("#clean_all").css('display', 'none');
    $('.WB_tab_b').append("<div style='margin-left: 16px;height: 26px;line-height: 26px;'><i></i>Loading：<span id='clean-loading'></span></div>");
    $('.WB_tab_b').append("<div style='margin-left: 16px;line-height: 26px;'><i></i>Error：<div id='error_box' style='color: red'></div></div>");
    $('.WB_tab_b').append("<div style='margin-left: 16px;line-height: 26px;'><i></i>Success：<div id='success_box' style='color: green'></div></div>");
}